import matplotlib.pyplot as plt
from matplotlib import colors
import numpy as np
from scipy import ndimage
from matplotlib.widgets import Slider
import argparse


def runforest(N, g, nsteps, connect=1):  # Run Forest, run
    """Run a forest fire simulation in an N by N grid. For each timestep in nsteps a cell
    is selected at random: if the cell is empty, it has probability g of growing a tree;
    otherwise the cell already has a tree and has probability 1-g of lightning striking
    and starting a fire.
    N: size of grid (int)
    g: probability between 0 and 1
    nsteps: number of steps (int)
    connect: determines how the burn will spread 1 --> edge, 2 --> edge/corner (default=1)

    returns numpy array of N x N x total steps (total steps != nsteps)
    """
    trees = []
    forest = np.zeros((N, N), dtype=np.int)

    f_list = []
    f_list.append(np.copy(forest))

    for i in range(nsteps):
        # pick a site
        site = np.random.randint(N, size=(2))
        # there is no tree
        if forest[site[0], site[1]] == 0:
            # grow one with probability g
            if np.random.random() < g:
                forest[site[0], site[1]] = 1

            f_list.append(
                np.copy(forest)
            )  # append the forest whether or not a tree grew

        else:
            # lightning strikes with probability 1-g
            if np.random.random() < 1 - g:
                # lightning strikes -> burn site
                # s determines connectivity of burn
                s = ndimage.generate_binary_structure(2, connect)

                # origin of the fire
                burn = np.zeros((N, N), dtype=np.int)
                burn[site[0], site[1]] = 1
                new_burn = burn

                old_forest = np.copy(forest)  # save copy of forest

                while new_burn.any():  # while things are still burning

                    forest = np.add(new_burn, forest)  # burn the forest
                    a = ndimage.binary_dilation(burn, s).astype(
                        burn.dtype
                    )  # spread the burn

                    burn2 = (
                        np.logical_and(old_forest, a) * 1
                    )  # are there trees in the burn?

                    new_burn = np.add(burn2, burn * -1)  # did anything new burn?

                    burn = burn2  # step complete

                    f_list.append(np.copy(forest))

                mask = (forest == 2) * 1

                f_list.append(np.add(mask, forest))  # burn becomes "burnt"
                forest = np.add(-2 * mask, forest)
                f_list.append(np.copy(forest))  # burnt resets to empty
        trees.append(np.sum(forest, dtype=np.int32))
    return np.dstack(f_list), trees


def prettyslider(steps, trees):
    """Create a slider widget using matplotlib to display the forest fire simulation.
    Input is 3D numpy array where steps proceed on 3rd dimension."""

    cmap = colors.ListedColormap(["tan", "green", "red", "black"])
    bounds = [0, 1, 2, 3, 4]
    norm = colors.BoundaryNorm(bounds, cmap.N)

    fig, (ax1, ax2) = plt.subplots(1, 2)

    l = ax1.imshow(steps[:, :, 0], cmap=cmap, norm=norm)
    plt.axis("off")

    axT = fig.add_axes([0.2, 0.95, 0.65, 0.03])

    sliderT = Slider(axT, "step", 0, steps.shape[2] - 1, valinit=0, valfmt="%i")

    def update(val):
        i = int(sliderT.val)

        im = steps[:, :, i]
        l.set_data(im)
        fig.canvas.draw_idle()

    sliderT.on_changed(update)

    ax2.plot(trees)

    plt.show()


if __name__ == "__main__":
    """docstring??"""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "-N", "--grid_length", type=int, required=True, help="Side length of the grid"
    )

    parser.add_argument(
        "-g",
        "--burn_probability",
        type=float,
        required=True,
        help="Probability between 0 and 1 that an empty cell with grow a tree",
    )

    parser.add_argument(
        "-n",
        "--number_steps",
        type=int,
        required=True,
        help="Number of timesteps to run the simulation",
    )

    parser.add_argument(
        "-c",
        "--connectivity",
        type=int,
        default=1,
        required=False,
        help="Connectivity of the burn 1 -> edge, 2 -> edge/corner",
    )

    args = parser.parse_args()

    allsteps, trees = runforest(
        args.grid_length, args.burn_probability, args.number_steps, args.connectivity
    )

    # print(trees)

    prettyslider(allsteps, trees)
